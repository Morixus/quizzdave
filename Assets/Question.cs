﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Question", menuName = "Question")]
public class Question : ScriptableObject
{
    public string question;
    public string[] answers = new string[4];

    [Range(1, 4)]
    public int correctAnswer = 1;

}
