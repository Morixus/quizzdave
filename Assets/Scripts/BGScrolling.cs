﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScrolling : MonoBehaviour
{
    private float _bgSpeed = 0.1f;
    //prywatne + serializacja
    [SerializeField] private Renderer _bgRend;

    void Update()
    {
        _bgRend.material.mainTextureOffset += new Vector2(0f, _bgSpeed * Time.deltaTime); // Zapętlanie ruchomego tła
    }
}