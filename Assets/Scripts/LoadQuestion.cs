﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoadQuestion : MonoBehaviour
{
    //a gdzie _?
    [SerializeField] private Question _question;
    [SerializeField] private TextMeshProUGUI _questionText;
    [SerializeField] private TextMeshProUGUI[] _answers = new TextMeshProUGUI[4];
  
    private void Start()
    {
        for (int i = 0; i < _answers.Length; i++)
        {
            _answers[i].SetText(_question.answers[i]);
        }
        _questionText.SetText(_question.question);
    }
}