﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private float _speed = 5f;
    private Vector3 _movement;
    private Rigidbody2D _rb;

    private void Awake()
    {
        //GetComponent = Awake
        _rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        _movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
    }
    private void FixedUpdate()
    {
        MoveCharacter(_movement);
    }

    private void MoveCharacter(Vector3 direction)
    {
        _rb.velocity = direction * _speed;
    }
}