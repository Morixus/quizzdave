﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;

public class QuestionManager : MonoBehaviour
{
    private List<int> _range;
    private int _randomNumber;
    private int _randomIndex;
    private int _goodCount;
    private int _wrongCount;
    private Rigidbody2D _rb;
    private Vector3 _movement;

    [SerializeField] private Question[] _questionArray;
    [SerializeField] private TextMeshProUGUI _questionText;
    [SerializeField] private TextMeshProUGUI[] _answers = new TextMeshProUGUI[4];
    [SerializeField] private TextMeshProUGUI _goodText;
    [SerializeField] private GameObject _goodAnswer;
    [SerializeField] private GameObject _wrongAnswer;

    private float _speed = 1.5f;
    private float _speedUp = 3f;
    private float _waitTime = 2f;
    private float _winAnswer = 20f;

    private void Start()
    {
        _goodCount = GameMaster.goodCount;
        _wrongCount = GameMaster.wrongCount;
        _rb = GetComponent<Rigidbody2D>();

        LoadQuestions();
        UpdateUI();
        SetCountText();
    }

    void Update()
    {
        // Poruszanie UI tj. teksów, triggerów
        transform.Translate(new Vector3(0, -_speed, 0) * Time.deltaTime);
        // Przyspieszanie poruszania UI
        //horizontal i vertical
        _movement = new Vector3(0f, Input.GetAxis("Vertical"), 0f);
        SaveVar();
    }

    void FixedUpdate()
    {
        MoveCharacter(_movement);
    }

    void MoveCharacter(Vector3 direction)
    {
        _rb.velocity = direction * _speedUp;
    }

    private void LoadQuestions()
    {
        Object[] _questions = Resources.LoadAll("Questions", typeof(Question));
        _questionArray = new Question[_questions.Length];

        for (int i = 0; i < _questions.Length; i++)
        {
            _questionArray[i] = (Question)_questions[i];
        }
        _range = Enumerable.Range(0, _questionArray.Length).ToList();
    } // Wrzucanie pytań z folderu do tablicy.

    private int RandomNumber()
    {
        _randomIndex = Random.Range(0, _range.Count - 1);
        return _range[_randomIndex];
    } // Losowanie pytania ze wszystkich dostępnych pytań.

    public void UpdateUI()
    {
        _randomNumber = RandomNumber();
        _range.RemoveAt(_randomIndex);

        _questionText.text = _questionArray[_randomNumber].question;
        for (int i = 0; i < _answers.Length; i++)
        {
            _answers[i].text = _questionArray[_randomNumber].answers[i];
        }
    } // Wyłączanie pytań na które odpowiedzieliśmy z puli pytań do wylosowania. Wrzucanie dostępnych odpowiedzi na wylosowane pytania do tablicy.

    public void CheckingAnswer(int triggerNumber)
    {
        if (triggerNumber == _questionArray[_randomNumber].correctAnswer)
        {
            StartCoroutine(GoodAnswer());
            //magic number
            _goodCount++;
            SetCountText();
        }
        else
        {
            StartCoroutine(WrongAnswer());
            _wrongCount++;
        }
    } // Sprawdzanie czy odpowiedź którą wybraliśmy jest dobra lub zła i dodawanie punktów do danege wyniku.

    private IEnumerator GoodAnswer()
    {
        _goodAnswer.SetActive(true);
        yield return new WaitForSeconds(_waitTime);
        _goodAnswer.SetActive(false);
    } // Wyświetlanie grafiki po wybraniu dobrej odpowiedzi.

    private IEnumerator WrongAnswer()
    {
        _wrongAnswer.SetActive(true);
        yield return new WaitForSeconds(_waitTime);
        _wrongAnswer.SetActive(false);
    } // Wyświetlanie grafiki po wybraniu złęj odpowiedzi.

    void SetCountText()
    {
        //rozdzielic na dwa texty
        _goodText.text = _goodCount.ToString();
        if (_goodCount >= _winAnswer)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    } // Wyświetlanie ilości dobrych odpowiedzi i przenoszenie na scene końcową po uzyskaniu określonego wyniku.

    public void SaveVar()
    {
        GameMaster.goodCount = _goodCount;
        GameMaster.wrongCount = _wrongCount;
    } // Zapisywanie wyniku do zmiennej, która przechodzi na scenę końcową.
}