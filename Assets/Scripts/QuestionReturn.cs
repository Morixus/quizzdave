﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionReturn : MonoBehaviour
{
    [SerializeField] private Transform _questionFrame;
    [SerializeField] private QuestionManager _questionMan;
    [SerializeField] private GameObject[] _mouses;
    [SerializeField] private GameObject[] _answers;

    private Vector3 _spawnPosition = new Vector3(0, 6.15f, -6.77f);

    private void OnTriggerEnter(Collider other)
    {
        //nie po stringach
        if(other.GetComponent<QuestionBord>())
        {
            _questionFrame.position = _spawnPosition;
            _questionMan.UpdateUI();
            for (int i = 0; i < _mouses.Length; i++)
            {
                _mouses[i].SetActive(true);
                _answers[i].SetActive(true);
            }
        } // Cofanie poruszającego się UI do pozycji początkowej po wejściu w trigger. Wrzucanie do obiektów nowe wylosowane pytanie. Włączanie wszystkich obiektów reprezentujących odpowiedzi
    }
}