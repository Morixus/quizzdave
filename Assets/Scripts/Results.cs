﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Results : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _goodText;
    [SerializeField] private TextMeshProUGUI _wrongText;

    private void Start()
    {
        _goodText.text = GameMaster.goodCount.ToString(); // Wyświetlanie ilości dobrych odpowiedzi
        _wrongText.text = GameMaster.wrongCount.ToString(); // Wyświetlanie ilości złych odpowiedzi
    }
}
