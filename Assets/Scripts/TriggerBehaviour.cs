﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBehaviour : MonoBehaviour
{
    [SerializeField] private int _triggerNumber;
    [SerializeField] private QuestionManager _questionMan;
    [SerializeField] private GameObject _answer;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<PlayerController>())
        {
            _questionMan.CheckingAnswer(_triggerNumber);
            this.gameObject.SetActive(false);
            _answer.SetActive(false);
        }
    } // Wywoływanie funkcji sprawdzającej czy dana odpowiedź jest prawdziwa. Wyłączanie obiektów reprezentujących wybraną odpowiedź
}