# QuizzDave

Project created for one of the subjects at university.
Made by four people in one week.
The only "must have" was that it has to be some sort of quiz about Linux.
So we made a game about little cat running in the park and he has to answer
questions correctly to win.

# Kacper Lewandowicz:
*  Main menu
*  Added questions
*  Made questions pop out randomly
*  Assembled assets in Unity

**Turning off questions we already answered from the all questions pool. Putting answers to drawn questions into array.**

    public void UpdateUI()
    {
        _randomNumber = RandomNumber();
        _range.RemoveAt(_randomIndex);

        _questionText.text = _questionArray[_randomNumber].question;
        for (int i = 0; i < _answers.Length; i++)
        {
            _answers[i].text = _questionArray[_randomNumber].answers[i];
        }
    }

**Putting questions from folder into the array.**

    private void LoadQuestions()
    {
        Object[] _questions = Resources.LoadAll("Questions", typeof(Question));
        _questionArray = new Question[_questions.Length];

        for (int i = 0; i < _questions.Length; i++)
        {
            _questionArray[i] = (Question)_questions[i];
        }
        _range = Enumerable.Range(0, _questionArray.Length).ToList();
    } // Wrzucanie pytań z folderu do tablicy.

# Jakub Czarnecki:
*  Moving background
*  Counting good and bad answers
*  Added questions
*  Improve player movement

**Moving backround downwards and looping it.**

    public class BGScrolling : MonoBehaviour
    {
        private float _bgSpeed = 0.1f;
        //prywatne + serializacja
        [SerializeField] private Renderer _bgRend;

        void Update()
        {
            _bgRend.material.mainTextureOffset += new Vector2(0f, _bgSpeed * Time.deltaTime); // Zapętlanie ruchomego tła
        }
    }
